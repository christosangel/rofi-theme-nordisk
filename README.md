## rofi-theme-nordisk

This is a [rofi](https://github.com/davatorium/rofi) theme with the [nord theme](https://github.com/nordtheme/nord) colors.

It is a simple adaptation of the android notification theme by Rasi.

![nordisk.png](nordisk.png)

### Installation

First, clone the project:

```
$ git clone https://gitlab.com/christosangel/rofi-theme-nordisk

```
then copy the `.rasi` file to the `/usr/share/rofi/themes` directory:

```
sudo cp rofi-theme-nordisk/nordisk.rasi /usr/share/rofi/themes/

```
You are ready to select and accept this theme with the `rofi-theme-selector` command.

Feel free to fork, propose changes or chime in, any feedback is welcome.
